package com.beyondxscratch.mandaloreexpress.domain.search.spacetrain.fare

import com.beyondxscratch.mandaloreexpress.domain.EqualityShould

class FareOptionShould : EqualityShould<FareOption>